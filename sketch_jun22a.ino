
#include <Servo.h>
#include <SPI.h>
#include <RFID.h>

RFID rfid(10, 9);

byte kart_1[5] = {243,145,7,133,224};
byte kart_2[5] = {54,217,105,172,42};

Servo myservo;
boolean card;

int Button1 = 4;
int Button2 = 5;
 
int Led1 = 6;
int Led2 = 7;
int grados = 0;
int EstadoLed = 0;
int EstadoTarjeta = 0;
const int zumbador = 8;
int correr = 0;
int salida = 8;
int potenciometro = A0;
float lectura;

const int Trigger = 6;
const int Echo = 2;
long t;
long d;

void setup() {

  Serial.begin(9600);
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT); 
  digitalWrite(Trigger, LOW);
  
  SPI.begin();
  rfid.init();
  pinMode(Led1, OUTPUT);
  pinMode(Led2, OUTPUT);
  
  pinMode(salida, OUTPUT); 
  pinMode(potenciometro, INPUT);
  pinMode(Button1, INPUT_PULLUP);
  pinMode(Button2, INPUT_PULLUP); 
  myservo.attach(3);
}

void loop() {

  Button1 = digitalRead(4);
  Button2 = digitalRead(5);
  lectura = analogRead(potenciometro);
  BotonesEstadoEncenderLeds();
  IndicadorDePotencia();
  IndicadorDeDistancia();  

  if (rfid.isCard()){
    if (rfid.readCardSerial()){
    }
    
    for (int i = 1; i < 5; i++){
      
    if (rfid.serNum[0] == kart_1[0] && rfid.serNum[1] == kart_1[1] && rfid.serNum[2] == kart_1[2] && rfid.serNum[3] == kart_1[3] && rfid.serNum[4] == kart_1[4]){
        EstadoTarjeta = 1;
    }

     else if (rfid.serNum[0] == kart_2[0] && rfid.serNum[1] == kart_2[1] && rfid.serNum[2] == kart_2[2] && rfid.serNum[3] == kart_2[3] && rfid.serNum[4] == kart_2[4]){
        EstadoTarjeta = 2;
     }
    }    
    rfid.halt();
  }

  else{
    EstadoTarjeta = 0;
  }
  
  Serial.print(EstadoLed);
  Serial.print(","); 
  Serial.print(d); 
  Serial.print(","); 
  Serial.print(grados); 
  Serial.print(","); 
  Serial.print(EstadoTarjeta); 
  Serial.println();  
  delay(50);
}

void BotonesEstadoEncenderLeds(){  
  
  if(Button1 == LOW)
  {
    EstadoLed = 1;
  }  
  
  else if(Button2 == LOW)
  {
    EstadoLed = 2;
  }
  
  else
  {
    EstadoLed = 0;
  }
}

void IndicadorDePotencia(){   
  grados = map(lectura, 0, 1023, 0, 180);
  myservo.write(grados);  
}

void IndicadorDeDistancia(){
  digitalWrite(Trigger, HIGH);
  digitalWrite(Trigger, LOW);  
  t = pulseIn(Echo, HIGH);
  d = t/59;
}
